/*
* Copyright © 2021 John Marshall jm.dev@jmsoft.co.uk
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of 
* this software and associated documentation files (the “Software”), to deal in the 
* Software without restriction, including without limitation the rights to use, 
* copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
* Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
 
/*
* Example of use of Debouncer class.
* Digital pin 3 acts as a debounced switch to turn on and off the onboard LED.
*/


#include "debouncer.h"
const int LEDpin = 13;
Debouncer* pin3;

void setup()
{
    pin3 = new Debouncer(3);
    pin3->debounceDelayMillis = 500; // Set a really long delay so we can see the effect for testing.
                                        // We will have to hold/release the button continuously for this
                                        // time before the led will change. The default delay is 50ms.
}

void loop()
{
    // To check the state of the pin
    if (pin3->changed())
    {
        digitalWrite(LEDpin, pin3->currentState);
    }
}
