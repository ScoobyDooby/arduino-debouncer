/*
* Copyright © 2021 John Marshall jm.dev@jmsoft.co.uk
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of 
* this software and associated documentation files (the “Software”), to deal in the 
* Software without restriction, including without limitation the rights to use, 
* copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
* Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Arduino.h"
#include "debouncer.h"

#ifdef __cplusplus
extern "C" {
#endif

Debouncer::Debouncer(int pin)
{
    this->pin = pin;
    this->currentState = digitalRead(this->pin);
    pressStartMillis = millis();
}


bool Debouncer::changed()
{

    int reading = digitalRead(pin);

    bool readingChanged = reading != lastReading;
    lastReading = reading;
    if (readingChanged)
    {
        pressStartMillis = millis();
    }

    bool inDelayPeriod = millis() - pressStartMillis < debounceDelayMillis;
    if (inDelayPeriod)
    {
        return false;
    }
    // pin has stabilised
    bool stateChanged = reading != currentState;
    if (!stateChanged)
    {
        return false;
    }

    // State has changed
    currentState = reading;
    return true;
}

#ifdef __cplusplus
}
#endif